# Customise the MkDocs configuration: `mkdocs.yml`

By default, MkDocs creates URLs as *directories*. For example the `chapter.md` file will result in the `/chapter/` URL. The [`use_directory_urls` configuration option](https://www.mkdocs.org/user-guide/configuration/#use_directory_urls) (when set to `false`) allows to change this behaviour and create URLs as *files*. For example the `chapter.md` file would result in the `/chapter.html` URL. To enable this feature the following line needs to be added to the MkDocs configuration file `mkdocs.yml`:

```yaml
use_directory_urls: false
```

A more complete MkDocs configuration file `mkdocs.yml` would look something like this:

```yaml
site_name: Service Documentation
site_description: Documentation for Service
site_author: Service Documentation Authors
site_url: https://service-docs.web.cern.ch/

repo_name: GitLab
repo_url: https://gitlab.cern.ch/service/docs
edit_uri: 'blob/master/docs'

theme:
  name: material

nav:
  - Introduction: index.md

use_directory_urls: false
```

More information about this configuration file can be found in the [MkDocs user guide](https://www.mkdocs.org/user-guide/configuration/#introduction){target=_blank}.
